# React development task

In this task, you will implement a news article teaser. A teaser is a short introduction to an article. The teaser must
contain a topic (text), an image and a title (text). It should look similar to this picture:

![Example](example.png)

## Topic
The topic should be rendered in the upper left corner on top of the image. It should have its own style and should
automatically be rendered completely uppercased.

## Title
The headline should be placed below the image and have the same width as the image. At the end of the title, it should
be a counter that increases by 1 each time you click on the image.

## Image
The image should be rendered in the middle of the teaser, feel free to use logo.svg in the src folder.

## Restrictions
In order to complete the task, use javascript and the rendering library React.
All files you need to complete the task are already created, so you do not need to create anything new, but if you feel
that something is missing, feel free to create your own files. Since we like a nice looking teaser, we would like you to
style it to look nice.


## How to start
If you have not installed npm already, this should be done first. Then follow these instructions:

0. npm install
1. npm start

And BAM! A browser should start with the environment running.

## How to submit the task
* Fork this repository (https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
* Create a pull request from your forked repo to the master branch in this repository.

## Help
[React](https://reactjs.org/) - The React website with a lot of informations

[Create React App](https://github.com/facebook/create-react-app) - We used the "Create React App" package to create this
task

[Barometern.se](http://www.barometern.se/) - Inspiration for your teaser
